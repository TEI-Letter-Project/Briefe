function openSearchField() {
    document.getElementById('searchFieldFullText').style.display = 'block';
    document.getElementById('openSearchField').style.display = 'none';
    document.getElementById('closeSearchField').style.display = 'block';
}
function closeSearchField() {
    document.getElementById('searchFieldFullText').style.display = 'none';
    document.getElementById('openSearchField').style.display = 'block';
    document.getElementById('closeSearchField').style.display = 'none';
}
function refreshUrl(mySplitter) {
    history.pushState(null, '', location.href.split(mySplitter)[0]);
}
