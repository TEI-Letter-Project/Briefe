xquery version "3.1";


(:bsp uri: http://localhost:8080/exist/apps/TeiBriefe/main/start.html:)

import module namespace config = "http://exist.db/app/TeiBriefe/modules/config" at "modules/config.xqm";
declare variable $exist:path external; (: /main/start.html :)
declare variable $exist:resource external; (:start.html :)
declare variable $exist:controller external; (:/TeiBriefe :)
declare variable $exist:prefix external; (:/apps  :)
declare variable $exist:root external; (: xmldb:exist:///db/apps :)
declare variable $exist:widget := fn:tokenize($exist:path, '/')[2]; (:main:)

declare variable $exist:modules := ["main", "lists"];


if ($exist:path eq '') then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <redirect url="{request:get-uri()}/"/>
    </dispatch>
    
else if ($exist:path eq "/") then
    (: forward root path to index.xql :)
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <redirect url="main/view.html"/>
    </dispatch>

else if($exist:resource eq "main.html") then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <view>
                <forward url="{$exist:controller}/modules/view.xql"/>
            </view>
            <error-handler>
                <forward url="{$exist:controller}/error-page.html" method="get"/>
                <forward url="{$exist:controller}/modules/view.xql"/>
            </error-handler>
        </dispatch>
else if (ends-with($exist:resource, ".html")) then
    (: the html page is run through view.xql to expand templates :)
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <view>
            <forward url="{$exist:controller}/modules/view.xql"/>
        </view>
		<error-handler>
			<forward url="{$exist:controller}/error-page.html" method="get"/>
			<forward url="{$exist:controller}/modules/view.xql"/>
		</error-handler>
    </dispatch>

(: Resource paths starting with $shared are loaded from the shared-resources app :)
else if (contains($exist:path, "/$shared/")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="/shared-resources/{substring-after($exist:path, '/$shared/')}">
            <set-header name="Cache-Control" value="max-age=3600, must-revalidate"/>
        </forward>
    </dispatch>


else if (contains($exist:path, "/resources")) then
                if(ends-with($exist:resource, ".css")) then
                    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                        <cache-control cache="yes"/>
                        <forward url="{$config:webapp-root}/resources/css/{$exist:resource}" method="get"/>
                    </dispatch>
                else
                    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                        <cache-control cache="yes"/>
                    </dispatch>

else if(contains($exist:path, "/modules")) then
                        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                            <forward url="{$exist:controller}{$exist:path}">
                                <set-header name="Cache-Control" value="max-age=3600, must-revalidate"/>
                            </forward>
                        </dispatch>



else if(ends-with($exist:resource, ".css") or ends-with($exist:resource, ".js")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}{$exist:path}">
            <set-header name="Cache-Control" value="max-age=3600, must-revalidate"/>
        </forward>
    </dispatch>



else
    (: everything else is passed through :)
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <cache-control cache="yes"/>
    </dispatch>
