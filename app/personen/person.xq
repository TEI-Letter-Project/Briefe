xquery version "1.0";
declare namespace TEI = "http://www.tei-c.org/ns/1.0";
declare option exist:serialize "method=html5 media-type=text/html";


<html xmlns="http://www.w3.org/1999/xhtml" lang="de-DE">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="autor" content="Viktoriya Lebedynska"/>
		<link type="text/css" media="all" href="person.css" rel="stylesheet"> </link>
		<script type="text/javascript" src="../js/format.js"></script>
		<title>Personen</title>
	</head>

	<body>

		<div class="body">
			<div class="title-banner">
				<div class="container">
					<div  class="main-title"><h1 id="personen">Personen</h1><hr/></div>
				</div>
			</div>

			<div class="container">
				{
				for $persons in collection(string-join('/db/apps/TeiData/Xml/Personen'))
				return

					for $person in $persons//TEI:listPerson/TEI:person
					order by data($person//TEI:persName/TEI:surname/@birth/text())
					return
						<div class="list">
							<div class="row">
								<div class="col-md-3" id="person-banner">
									<!-- Überschrift für jede Person besteht aus (1) Nachnamen (bei Frauen name <birth> + <married>) und (2) Vornamen -->
									<li class="person" id="person-{$person/@xml:id}" >
										<!-- Der Link soll zur Korrespondenz führen-->
										<a href="#">
											<h3>
												{data($person//TEI:persName/TEI:surname/text())}
												&#32;
												{data($person//TEI:persName/TEI:forename/text())}
											</h3>
										</a>
									</li>
								</div>
							</div>



							<div class="row">
								<!--TODO Fotos richtig verknüpfen-->
								<div class="col-md-1">
									<img class="pers-img" src="../images/view/Dummi.jpg" />
								</div>

								<div class="col-md-4 ">

										<ul class="list" id="person-{$person/@xml:id}">
											<li>{data($person//TEI:sex/text())}</li>
											<li>* {data($person//TEI:birth/TEI:date/text())} in {data($person//TEI:birth/TEI:name/text())}</li>
											<li>&#10013; {data($person//TEI:death/TEI:date/text())} in {data($person//TEI:birth/TEI:name/text())} </li>
											<li>Gelernter Beruf: {data($person//TEI:occupation/text())}</li>
											<li>Beschäftigung im Krieg: {data($person//TEI:persName/TEI:roleName/text())}
												bei {data($person//TEI:affiliation/TEI:orgName/text())}</li>
											<br/>
										</ul>

								</div>

								<!-- In allen TEI suchen nach key-id, Ausgabe der Titel aller TEI, in denen die Person gefunden wurde -->
								<!-- chronologisch sortiert-->
								<div class="col-md-7 ">
									<div class="person" id="person-{$person/@xml:id}">
										<em><a href="#">Korrespondenz anzeigen</a></em>
										<div id="person-content" class="person-{$person/@xml:id}">
											<!-- In allen TEI suchen nach key-id, Ausgabe der Titel aller TEI, in denen die Person gefunden wurde -->
											<!-- chronologisch sortiert-->
											{
												(: der Link muss später mit der richtigen Datenbank verknüpft werden:)
												for $letter in collection("/db/apps/briefe/xml-tei")
												for $search in $letter//TEI:persName/@key
												where data($search)=$person/@xml:id
												order by data($letter//TEI:title/TEI:date/@when)
												return
													<ul>
														<li>
															<a href="{replace(base-uri($letter),'/db/','/exist/')}">{data($letter//TEI:title/TEI:date/@when)}
																<br/> {data($letter//TEI:title/text())}
															</a>
														</li>
													</ul>
											}

										</div>
									</div>
								</div>

							</div>
						</div>
}

			</div>
		</div>

	</body>

</html>



