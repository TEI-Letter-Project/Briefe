

//Die xquery-Datei, die Personen aus XML-Dateien ausliest, wird geladen.
function loadDoc() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("container_personen").innerHTML = this.responseText;
        }
    };
    xhttp.open("GET", "person.xq", true);
    xhttp.send();
}




$(document).ready(function(){

//Einbinden von Header und Footer
    $(function(){
        $("[data-load]").each(function(){
            $(this).load($(this).data("load"), function(){

            });
        });
    });


//Darstellung Brief/Postkarte - Zweispaltige Anzeige laden
    $(function() {
        $("#text-active").show();
        $("#image-active").show();


        $("#tabsId-l").on("click", "li", function(evt) {
            evt.preventDefault();
            var id = $(this).data("tabContent");
            $("#tabsContentId-l")
                .find(".tab-content[data-tab-content='" + id + "']").show()
                .siblings().hide();
        });

        $("#tabsId-r").on("click", "li", function(evt) {
            evt.preventDefault();
            var id = $(this).data("tabContent");
            $("#tabsContentId-r")
                .find(".tab-content[data-tab-content='" + id + "']").show()
                .siblings().hide();
        });

        $("ul.nav li").on( 'click', function() {
            $( this ).parent().find( 'li.active' ).removeClass( 'active' );
            $( this ).addClass( 'active' );
        });

    });


// Personen - Zugehörige Korrespondenz onclick laden
    $(function(){
        var id = $("#person-content.class").hide();
        console.log(id);


        //$("#person-content").hide();

        $(".person").on("click", function(e){
            e.preventDefault();
            console.log(this.id);
            var person = this.id;
            console.log("."+person);
            $("."+person).toggle();



        });

    });


// Content loaden

    loadDoc();

});