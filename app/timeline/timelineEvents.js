// Source:
// http://www.bpb.de/geschichte/deutsche-geschichte/der-zweite-weltkrieg/203726/chronologische-uebersicht-der-zweite-weltkrieg

const timelineEvents = [
    { start: '1933-01-30', content: 'Reichspräsident Hindenburg ernennt Hitler zum Reichskanzler', className: 'event' },
    { start: '1936-03-07', content: 'Deutsche Truppen beginnen den Einmarsch in das Rheinland', className: 'event' },
    { start: '1936-08-01', content: 'Beginn der olympischen Sommerspiele in Berlin', className: 'event' },
    { start: '1936-08-16', content: 'Ende der olympischen Sommerspiele in Berlin', className: 'event' },
    { start: '1938-03-12', content: 'Deutscher Einmarsch in Österreich', className: 'event' },
    { start: '1938-11-09', content: 'Novemberpogrom gegen Juden im Deutschen Reich ("Reichskristallnacht")', className: 'event' },
    { start: '1939-09-01', content: 'Deutscher Angriff auf Polen', className: 'event' },
    { start: '1939-09-03', content: 'Kriegserklärung Großbritanniens und Frankreichs an Deutschland', className: 'event' },
    { start: '1940-04-09', content: 'Beginn der Besetzung Norwegens und Dänemarks durch die Wehrmacht', className: 'event' },
    { start: '1940-05-10', content: 'Einmarsch der Wehrmacht in die Niederlande, Belgien und Luxemburg', className: 'event' },
    { start: '1940-12-29', content: 'Deutscher Luftangriff auf London', className: 'event' },
    { start: '1941-06-22', content: 'Deutscher Überfall auf die Sowjetunion', className: 'event' },
    { start: '1941-08-08', content: 'Erster sowjetischer Luftangriff auf Berlin', className: 'event' },
    { start: '1941-10-02', content: 'Beginn des Vormarsches auf Moskau', className: 'event' },
    { start: '1941-12-07', content: 'Japanischer Luftangriff auf Pearl Harbor', className: 'event' },
    { start: '1941-12-11', content: 'Kriegserklärung Deutschlands und Italiens an die USA', className: 'event' },
    { start: '1942-09-13', content: 'Beginn der fünfmonatigen Schlacht von Stalingrad', className: 'event' },
    { start: '1945-09-03', content: 'Unterzeichnung eines Waffenstillstandsabkommens zwischen den Westalliierten und Italien', className: 'event' },
    { start: '1944-06-06', content: 'Die Alliierten landen in der Normandie', className: 'event' },
    { start: '1944-10-21', content: 'Besetzung Aachens als erster deutscher Stadt', className: 'event' },
    { start: '1945-01-27', content: 'Das Konzentrationslager Auschwitz wird von Einheiten der Roten Armee befreit', className: 'event' },
    { start: '1945-02-13', content: 'Dresden wird durch britische und amerikanische Bombenangriffe zerstört', className: 'event' },
    { start: '1945-04-30', content: 'Die Rote Armee erobert das Zentrum Berlins', className: 'event' },
    { start: '1945-05-08', content: 'Kapitulation der Wehrmacht; Ende des Krieges in Europa', className: 'event' },
    { start: '1945-08-06', content: 'Abwurf einer Atombombe auf die japanischen Stadt Hiroshima', className: 'event' },
    { start: '1945-08-09', content: 'Abwurf einer Atombombe auf die japanischen Stadt Nagasaki', className: 'event' },
    { start: '1945-09-02', content: 'Kapitulation Japans', className: 'event' }
];
