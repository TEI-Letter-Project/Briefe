xquery version "3.1";

import module namespace database = "http://exist.db/app/TeiBriefe/modules/database" at "../modules/database.xqm";

declare namespace TEI = "http://www.tei-c.org/ns/1.0";
declare option exist:serialize "method=json media-type=text/javascript";


<root>
    { for $letter in $database:briefe return
        <result>
            <id>{$letter//TEI:msIdentifier/TEI:idno/text()}</id>
            <content>{$letter//TEI:titleStmt/TEI:title/text()}</content>
            <start>{$letter//TEI:profileDesc/TEI:correspDesc/TEI:correspAction[@type="sent"]/TEI:date/@when/string()}</start>
            <className>letter</className>
        </result>
    }
    { for $postcard in $database:postkarten return
        <result>
            <id>{$postcard//TEI:msIdentifier/TEI:idno/text()}</id>
            <content>{$postcard//TEI:titleStmt/TEI:title/text()}</content>
            <start>{$postcard//TEI:profileDesc/TEI:correspDesc/TEI:correspAction[@type="sent"]/TEI:date/@when/string()}</start>
            <className>postcard</className>
        </result>
    }
</root>
