const minYear = 1933;
const maxYear = 1946;

const options = {
    height: '100%',
    min: minYear.toString(),
    max: maxYear.toString(),
    zoomMin: 400000000,
    locale: 'de',
    stack: true,
    margin: {
        item: {
            horizontal: -1
        }
    },
    order: (a, b) => a.id < b.id
};

let timeline;
let activeItemTypes = ['letter', 'postcard', 'event'];
let items = [];
let clusters = [];
let showClusters = true;


$.getJSON('timelineData.xq', initialize);


function initialize(data) {

    initializeData(data);
    initializeTimeline();
    initializeOptionsMenu();
}


function initializeData(data) {

    if (!data) return;

    items = Array.isArray(data.result) ? data.result : [data.result];
    items = items.filter(item => item.start !== null).concat(timelineEvents);

    createClusters();
}


function initializeTimeline() {

    timeline = new vis.Timeline($('#timeline-container').get(0), new vis.DataSet(clusters), options);
    timeline.on('select', selectItem);
    timeline.on('rangechange', updateClustering);

    const selectionId = getIdFromUrl();
    if (selectionId) zoomToItem(selectionId);
}


function createClusters() {

    for (let item of items) {
        if (item.className === 'event') continue;
        addToCluster(item);
    }

    addClusterLabels();
}


function addToCluster(item) {

    const year = new Date(item.start).getFullYear();
    const cluster = getCluster(item, year);

    if (cluster) {
        cluster.content += 1;
    } else {
        clusters.push(createCluster(item, year));
    }
}


function getCluster(item, year) {

    return clusters.find(cluster => {
        return cluster.start === year + '-01-01' && cluster.className === item.className;
    });
}


function createCluster(item, year) {

    return {
        id: 'cluster-' + item.className + '-' + year + '-01-01',
        start: year + '-01-01',
        end: year + '-12-31',
        content: 1,
        className: item.className
    };
}


function addClusterLabels() {

    for (let cluster of clusters) {
        if (cluster.content === 1) {
            if (cluster.className === 'letter') {
                cluster.content = 'Ein Brief';
            } else if (cluster.className === 'postcard') {
                cluster.content = 'Eine Postkarte';
            } else {
                cluster.content = 'Ein Ereignis';
            }
        } else {
            cluster.content = cluster.content.toString();
            if (cluster.className === 'letter') {
                cluster.content += ' Briefe';
            } else if (cluster.className === 'postcard') {
                cluster.content += ' Postkarten';
            } else {
                cluster.content += ' Ereignisse';
            }
        }
    }
}


function updateClustering(rangeChange) {

    const timelineWindow = getCurrentTimelineWindowInDays(rangeChange);

    if (showClusters && timelineWindow <= 365) {
        showClusters = false;
        updateItems();
    } else if (!showClusters && timelineWindow > 365) {
        showClusters = true;
        updateItems();
    }
}


function getCurrentTimelineWindowInDays(rangeChange) {

    const startDate = new Date(rangeChange.start);
    const endDate = new Date(rangeChange.end);

    return Math.ceil(Math.abs(endDate.getTime() - startDate.getTime()) / (1000 * 3600 * 24));
}


function initializeOptionsMenu() {

    listenToChanges($('#letterCheckbox'));
    listenToChanges($('#postcardCheckbox'));
    listenToChanges($('#eventCheckbox'));
    setTimeout(() => { $('#timeline-options').addClass('show'); }, 1000);
}


function listenToChanges(checkbox) {

    checkbox.change(event => {
        if (event.target.checked) {
            showItems(event.target.value);
        } else {
            hideItems(event.target.value);
        }
    });
}


function showItems(type) {

    if (!activeItemTypes.includes(type)) activeItemTypes.push(type);
    updateItems();
}


function hideItems(type) {

    if (activeItemTypes.includes(type)) activeItemTypes.splice(activeItemTypes.indexOf(type), 1);
    updateItems();
}


function updateItems() {

    const timelineItems = showClusters ? clusters : items;
    timeline.setItems(new vis.DataSet(timelineItems.filter(item => activeItemTypes.includes(item.className))));
}


function selectItem(selection) {

    const item = getItemFromSelection(selection);
    if (!item || item.className === 'event') return;

    if (item.id.includes('cluster')) {
        timeline.setWindow(item.start, item.end);
    } else {
        window.location.href = getSelectionLink(item);
    }
}


function getItemFromSelection(selection) {

    if (selection.items.length !== 1) return undefined;

    return getItem([].concat(items).concat(clusters), selection.items[0]);
}


function getItem(items, id) {

    return items.find(item => item.id === id);
}


function getSelectionLink(item) {

    const mode = item.className === 'letter' ? 'Briefe' : 'Postkarten';
    return '../itemview/view.html?mode=' + mode + '&item=' + item.id;
}


function getIdFromUrl() {

    const splitString = location.search.split('=');

    if (splitString[0] === '?item' && !splitString[1].includes('=')) {
        return splitString[1];
    }
}


function zoomToItem(id) {

    const item = getItem(items, id);
    if (!item) return;

    const startDate = new Date(item.start);
    startDate.setMonth(startDate.getMonth() - 1);
    const endDate = new Date(item.start);
    endDate.setMonth(endDate.getMonth() + 1);

    timeline.setWindow(startDate, endDate, { animation: false });
}