xquery version "3.0";

(:~
: User: jan
: Date: 08.08.18
: Time: 23:04
: To change this template use File | Settings | File Templates.
:)

module namespace database = "http://exist.db/app/TeiBriefe/modules/database";

import module namespace templates="http://exist-db.org/xquery/templates";
import module namespace config="http://exist.db/app/TeiBriefe/modules/config" at "config.xqm";

declare variable $database:app-root := $config:webapp-root;
declare variable $database:data-web-root := $config:webapp-data;
declare variable $database:data-root := $config:data-path;

declare variable $database:database-image-collection-link := concat($database:data-web-root,"/Images");
declare variable $database:database-xml-collection-link := concat($database:data-web-root,"/Xml");

declare variable $database:database-xml-collection-path := concat($database:data-root, "/Xml");
declare variable $database:database-image-collection-path := concat($database:data-root, "/Images");

declare variable $database:database-xml-personen-collection-path := concat($database:database-xml-collection-path, "/Personen");
declare variable $database:database-xml-orte-collection-path := concat($database:database-xml-collection-path, "/Orte");


declare variable $database:briefe := collection(concat($database:database-xml-collection-path,"/Briefe"));
declare variable $database:postkarten := collection(concat($database:database-xml-collection-path, "/Postkarten"));
declare variable $database:personen := collection($database:database-xml-personen-collection-path);
declare variable $database:orte := collection($database:database-xml-orte-collection-path);

declare function database:src($node as node(), $model as map(*)) as xs:string{ concat($database:database-image-collection-link,'/')};

declare function database:src-light($itemname as xs:string) as xs:string{ concat($database:database-image-collection-link,'/', $itemname)};

declare function database:database-xml-item($type as xs:string, $itemname as xs:string ) as node(){
    doc(concat($database:database-xml-collection-path, "/",$type,"/", $itemname,".xml"))
};

declare function database:database-xsl-item($itemname as xs:string) as node(){
    doc(concat($database:database-xml-collection-path, "/xsl/", $itemname,".xsl"))
};

declare function database:create-id($namestring as xs:string, $type as xs:string )as xs:string{
    let $time := current-dateTime()
    return concat($type,util:hash(concat($namestring, $time),"md5"))
};

declare function database:store-xml($data as node(), $name as xs:string, $type as xs:string ){
    let $login := xmldb:login("/db", $config:admin, $config:admin-pw)
    let $path := concat($database:database-xml-collection-path,"/",$type,"/")
    let $xmlName := concat($name,".xml")
    let $store :=  xmldb:store($path, $xmlName ,$data)
    return $store
};

declare function database:store-binary($data as item(), $name as xs:string, $itemType as xs:string){
    let $login := xmldb:login("/db", $config:admin, $config:admin-pw)
    let $path := concat($database:database-image-collection-path, "/")
    let $itemName := concat($name, ".", $itemType)
    let $store := xmldb:store($path, $itemName, $data, "image/jpeg")
    return $store
};




