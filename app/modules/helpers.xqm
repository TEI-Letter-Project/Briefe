xquery version "3.0";

(:
   Author = Benjaminn Bigalke
   modified by Jan Bigalke
:)


module namespace helpers="http://exist.db/app/TeiBriefe/modules/helpers";

import module namespace templates="http://exist-db.org/xquery/templates";
import module namespace config="http://exist.db/app/TeiBriefe/modules/config" at "config.xqm";

import module namespace database="http://exist.db/app/TeiBriefe/modules/database" at "database.xqm";

declare namespace util="http://exist-db.org/xquery/util";
declare namespace request="http://exist-db.org/xquery/request";
declare namespace tei="http://www.tei-c.org/ns/1.0";

(: web-root of the app :)
declare variable $helpers:app-root := $config:webapp-root;
declare variable $helpers:request-path := $config:request-path;
declare variable $helpers:data-path := $config:data-path;

declare function helpers:app-root($node as node(), $model as map(*)){
 let $elname := $node/node-name(.)
 
 return if (xs:string($elname) = "link")
        then <link href="{$helpers:app-root}/{$node/@href}">
                {$node/@*[not(xs:string(node-name(.)) = "href")]}
             </link>
        else if (xs:string($elname) = "script" and $node/@type = "text/javascript")
        then <script type="{$node/@type}" src="{$helpers:app-root}/{$node/@src}" />
        else if (xs:string($elname) = "img")
        then <img src="{$database:database-image-collection-link}/{$node/@src}">
                {$node/@*[not(xs:string(node-name(.)) = "src")]}
             </img>
        else if (xs:string($elname) = "a")
             then <a href="{$helpers:app-root}/{$node/@href}">
                    {$node/@*[not(xs:string(node-name(.)) = "href")]}
                    {templates:process($node/node(), $model)}
                  </a>
        else if (xs:string($elname) = "form")
             then <form action="{$helpers:app-root}/{$node/@action}">
                    {$node/@*[not(name() = ("action"))]}
                    {templates:process($node/node(), $model)}
                  </form>
        else $node
};


declare function helpers:dynamic-class($node as node(), $model as map(*), $class as xs:string){
    let $elname := $node/node-name(.)
    let $url := request:get-url()
    let $tokenized := tokenize(request:get-url(), "/")
    let $module := $tokenized[count($tokenized)-1]

    return if(xs:string($elname) = "div")
        then
            <div class="{if($module != "main" and $class = "full-screen-intro") then concat($class,"-default") else $class}">
            {$node/@*[not(name() = ("class"))]}
            {templates:process($node/node(), $model)}
        </div>
    else()

};


declare function helpers:app-root-img($node as node(), $model as map(*)){
    let $elname := $node/node-name(.)

    return  <img src="{$helpers:app-root}/{$node/@src}">
                    {$node/@*[not(xs:string(node-name(.)) = "src")]}
                </img>

};


declare function helpers:mainBanner($node as node(), $model as map(*)){
    let $url := request:get-url()
    let $tokenized := tokenize(request:get-url(), "/")
    let $module := $tokenized[count($tokenized)-1]
    return  if($module = "main") then
          <div class="intro-image">
            <div id="banner">
                <div>
                    <img class="img" data-template="helpers:app-root-img" src='../images/_Startbild.jpg'/>
                    <!--<div class="bottomleft">Field Mail during Second World War</div>-->
                </div>
            </div>
        </div>
    else()
};

declare function helpers:mainLogo($node as node(), $model as map(*)){
let $url := request:get-url()
let $tokenized := tokenize(request:get-url(), "/")
let $module := $tokenized[count($tokenized)-1]
return  if($module = "main") then
    <div class="logo"><p>Feldpost im Zweiten Weltkrieg</p></div>
else()
};

(:### Range Suche ###:)
declare function helpers:range-MultiStats($db as node()*, $name as xs:string*, $case as xs:string*,$content as xs:string*) as node()* {
    let $name := concat('("',string-join($name,'","'),'")')
    let $case := concat('("',string-join($case,'","'),'")')
    let $content := concat('"',string-join($content,'","'),'"')
    let $search-funk := concat('//range:field(',$name,',',$case,',',$content,')')
    let $search-build := concat("$db",$search-funk)
    return util:eval($search-build)
};
(:~
 : Eine each Funktion zur Verwendung in views, identisch mit templates:each, 
 : aber ohne %templates:wrap Annotation, also ohne zusätzliches umschließendes
 : $node - Element
 :)
declare function helpers:each($node as node(), $model as map(*), $from as xs:string, $to as xs:string) {
    for $item in $model($from)
    return
        element { node-name($node) } {
            $node/@*, templates:process($node/node(), map:new(($model, map:entry($to, $item))))
        }
};

(:~
 : Identisch zu helpers:each aber ganz ohne umschließendes Elemente
 :)
declare function helpers:invisibleEach($node as node(), $model as map(*), $from as xs:string, $to as xs:string) {
    for $item in $model($from)
    return
        templates:process($node/node(), map:new(($model, map:entry($to, $item))))
};


declare function helpers:getValue($node as node(), $model as map(*), $key as xs:string) {
    $model($key)
};

 declare function helpers:getValueMap($node as node(), $model as map(*),$container as xs:string,$key as xs:string) {
    $model($container)($key)
 };

declare function helpers:createValueMap($node as node(), $model as map(*), $container as xs:string, $key as xs:string,$name as xs:string) {
    map:new(($model, map:entry($name, $model($container)($key))))
};

declare function helpers:createValueMapEach($node as node(), $model as map(*), $container as xs:string, $key as xs:string,$name as xs:string) {
    for $item in $model($container)($key)
    return
        templates:process($node/node(),map:new(($model, map:entry($name, $item))))
};

declare function helpers:contains-any-of( $arg as xs:string?, $searchStrings as xs:string* )  as xs:boolean {

   some $searchString in $searchStrings
   satisfies contains($arg,$searchString)
 } ;
 
 
declare function helpers:index-of-node( $nodes as node()*, $nodeToFind as node() )  as xs:integer* {
  for $seq in (1 to count($nodes))
  return $seq[$nodes[$seq] is $nodeToFind]
 } ;


declare function helpers:openDoc ($doc  as xs:string) as node()* {
    doc(concat($helpers:data-path,$doc,".xml"))
};
