xquery version "3.1";

(:~
: User: jan
: Date: 07.09.18
: Time: 14:46
: To change this template use File | Settings | File Templates.
:)

import module namespace database = "http://exist.db/app/TeiBriefe/modules/database" at "../modules/database.xqm";
import module namespace JsonToXml = "http://localhost:8080/exist/apps/briefe/JsonToXml" at "JsonToXml.xq";

<div>
    {
        let $jsonstring := request:get-parameter('json','')
        let $type := request:get-parameter('type','')

        let $xml := if($type = "letter") then
            let $name := database:create-id("newXml", "b")
            let $xml := JsonToXml:start($jsonstring, $name)
            let $store := database:store-xml($xml, $name, "Briefe")
            return $name
            else(
                let $name := database:create-id("newXml", "k")
                let $xml := JsonToXml:start($jsonstring, $name)
                let $store := database:store-xml($xml, $name, "Postkarten")
                return $name
            )

        return $xml
    }
</div>


