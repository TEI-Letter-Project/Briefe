xquery version "3.1";

module namespace JsonToXml = "http://localhost:8080/exist/apps/briefe/JsonToXml";

import module namespace database = "http://exist.db/app/TeiBriefe/modules/database" at "../modules/database.xqm";

declare namespace saxon = "http://saxon.sf.net/";
declare namespace TEI = "http://www.tei-c.org/ns/1.0";


declare function JsonToXml:createTranscription($transcriptionArray){

    let $xml :=
        <TEI:div type="letter">
            {
                let $string := ("&lt;TEI:p xmlns:TEI='http://www.tei-c.org/ns/1.0'&gt; ",
                for $i in (1 to array:size($transcriptionArray))
                    let $map := array:get($transcriptionArray, $i)
                    (:let $transcriptionString := replace(map:get($map, 'transcriptedText'),"\[p\]","&lt;TEI:rs type='person'&gt;"):)
                    (:let $transcriptionString := replace($transcriptionString, "\[/p\]","&lt;/TEI:rs&gt;"):)
                    let $transcriptionString := JsonToXml:createTags(map:get($map, 'transcriptedText'))
                    let $lines := tokenize($transcriptionString, "\[/n\]")
                    return ("&lt;TEI:pb facs='",map:get($map, 'imageFileURL'),"'/&gt;",
                        for $line at $pos in $lines
                return ("&lt;TEI:lb n='",$pos - 1,"'/&gt;", $line)
                ),"&lt;/TEI:p&gt;")

                return $string
            }
        </TEI:div>

    let $Teixml := <TEI:div type="letter">
        {
            util:parse($xml/text())
        }</TEI:div>

    return $Teixml
};



declare function JsonToXml:createTags($transcriptionString){
    let $resultstring := if(matches($transcriptionString, "\[p\s?[A-Za-z]*\][\w.*]+\[/p\]")) then(
        let $element := replace($transcriptionString, "^.*(\[p\s?[A-Za-z]*\][\w.*]+\[/p\]).*$","$1")
        let $type := replace($element, "^.*\[([a-z]).*$","$1")
        let $name := if(matches($element, "\[[a-z]\s([a-zA-Z]+)")) then (
            replace($element, "^.*\[[a-z]\s([a-zA-Z]+).*$","$1"))
        else()
        let $content := replace($element,"^.*\[[a-z]\s?[a-zA-Z]*\](.+)\[.*$","$1")

        let $newstring := if(empty($name))then (concat("&lt;TEI:rs type='person'&gt;",$content,"&lt;/TEI:rs&gt;"))
        else (concat("&lt;TEI:rs type='person' subtype='",$name,"'&gt;",$content,"&lt;/TEI:rs&gt;"))
        let $test := replace($element, "\[","\\[")
        let $test2 := replace($test, "\]","\\]")
        let $result := replace($transcriptionString, $test2, $newstring)
        return $result)
    else()

    return if(empty($resultstring)) then $transcriptionString else (JsonToXml:createTags($resultstring))


};

declare function JsonToXml:createImages($transcriptionArray){
    let $xml :=
        for $i in (1 to array:size($transcriptionArray))
        let $map := array:get($transcriptionArray, $i)
        return <TEI:graphic url="{map:get($map, 'imageFileURL')}"></TEI:graphic>

    return $xml
};

declare function JsonToXml:create_corresp_received($persXml, $locationXml){

    let $person := $persXml//TEI:person

    let $name := JsonToXml:create_name($person)

    let $place := $locationXml//TEI:place

    let $xml :=<TEI:correspAction type="received">
        <TEI:persName key="{string($person/@xml:id)}">{$name}</TEI:persName>
        <TEI:settlement key="{string($place/@xml:id)}">{$place//TEI:placeName/text()}</TEI:settlement>
    </TEI:correspAction>

    return $xml

};

declare function JsonToXml:create_corresp_sent($persXml, $locationXml, $date){

    let $person := $persXml//TEI:person

    let $name :=  JsonToXml:create_name($person)

    let $place := $locationXml//TEI:place

    let $xml :=<TEI:correspAction type="sent">
        <TEI:persName key="{string($person/@xml:id)}">{$name}</TEI:persName>
        <TEI:settlement key="{string($place/@xml:id)}">{$place//TEI:placeName/text()}</TEI:settlement>
        <TEI:date when="{$date}">{$date}</TEI:date>
    </TEI:correspAction>

    return $xml

};

declare function JsonToXml:create_title($senderXml, $receiverXml, $date, $type){


    let $art := if($type = "letter") then("Brief")

    else if($type = "postCard") then ("Postkarte")
        else ()

    let $sendername := if(empty($senderXml)) then "Unbekannt" else JsonToXml:create_name($senderXml)
    let $receivername := if(empty($receiverXml)) then "Unbekannt" else    JsonToXml:create_name($receiverXml)
    return <TEI:title>{concat($art, " von ", $sendername, " an ", $receivername)}<TEI:date when="{$date}">{$date}</TEI:date></TEI:title>
};

declare function JsonToXml:create_name($nameXml) as xs:string{
    let $name := if(not(empty($nameXml//TEI:surname[@type="married"]))) then concat($nameXml//TEI:forename," ", $nameXml//TEI:surname[@type="married"])
    else(concat($nameXml//TEI:forename, " ", $nameXml//TEI:surname[@type="birth"]))


    return if(empty($name) || $name = "") then "Unbekannt" else $name

};

declare function JsonToXml:create_settlement_name($settlementXml) as xs:string{
    let $place := $settlementXml//TEI:place/TEI:placeName/text()
    return $place
};

declare function JsonToXml:start($jsonString as xs:string, $idno as xs:string){

    let $json := replace($jsonString, '\\n', "[/n]")

    let $options := map {"liberal" :true(), "duplicates": "use-last"}
    let $parsed := parse-json($json, $options)



    let $transcriptions := map:get($parsed,"transcriptions")
    let $transcriptionxml := JsonToXml:createTranscription($transcriptions)

    let $receiverXml :=
        if(map:get($parsed,'receiverID')) then
            database:database-xml-item("Personen", map:get($parsed, 'receiverID'))
        else()

    let $receiverOrtXml :=
        if(map:get($parsed, 'receiverPlace') != '') then
            database:database-xml-item("Orte", map:get($parsed, 'receiverPlace'))
        else()

    let $sentPersonXml :=
        if(map:get($parsed,'senderID')) then
            database:database-xml-item("Personen", map:get($parsed,'senderID'))
        else()

    let $sentLocationXml :=
        if(map:get($parsed, 'senderPlace')) then database:database-xml-item("Orte", map:get($parsed, 'senderPlace'))
        else()


    let $xml :=
        <TEI:TEI xmlns:TEI="http://www.tei-c.org/ns/1.0">
            <TEI:teiHeader>
                <TEI:fileDesc>
                    <TEI:titleStmt>
                        {JsonToXml:create_title($sentPersonXml, $receiverXml, map:get($parsed, 'shippingDate'), map:get($parsed, 'type'))}
                    </TEI:titleStmt>
                    <TEI:editionStmt>
                        <TEI:edition/>
                        <TEI:respStmt>
                            <TEI:resp>Transkript</TEI:resp>
                            <TEI:name>Editor</TEI:name>
                        </TEI:respStmt>
                    </TEI:editionStmt>
                    <TEI:publicationStmt>
                        <TEI:p>Intern</TEI:p>
                    </TEI:publicationStmt>
                    <TEI:sourceDesc>
                        <TEI:msDesc>
                            <TEI:msIdentifier>
                                <TEI:repository>o.A</TEI:repository>
                                <TEI:idno>{$idno}</TEI:idno>
                                <TEI:altIdentifier type="FPNR">
                                    <TEI:idno>{map:get($parsed, 'fieldPostNumber')}</TEI:idno>
                                </TEI:altIdentifier>
                            </TEI:msIdentifier>
                        </TEI:msDesc>
                    </TEI:sourceDesc>
                </TEI:fileDesc>{
                <TEI:profileDesc>
                    <TEI:correspDesc>
                        {JsonToXml:create_corresp_sent($sentPersonXml, $sentLocationXml, map:get($parsed, 'shippingDate'))}
                        {JsonToXml:create_corresp_received($receiverXml, $receiverOrtXml)}
                    </TEI:correspDesc>
                    {
                        if (map:get($parsed, 'mentionedPlaces') != '' or map:get($parsed, 'mentionedPersons') != '')
                        then
                            <TEI:settingDesc>
                                <TEI:listPlace>
                                    {
                                        let $mentionedPlacesArray := map:get($parsed, 'mentionedPlaces')
                                        for $i in (1 to array:size($mentionedPlacesArray))
                                        let $key := array:get($mentionedPlacesArray, $i)
                                        return <TEI:place><TEI:settlement key="{$key}">{JsonToXml:create_settlement_name(database:database-xml-item("Orte",$key))}</TEI:settlement></TEI:place>
                                    }
                                </TEI:listPlace>
                            </TEI:settingDesc>
                        else ()
                    }
                    {
                        if (map:get($parsed, 'mentionedPersons') != '')
                        then
                            <TEI:particDesc>
                                <TEI:listPerson>
                                    {
                                        let $mentionedPersonsArray := map:get($parsed, 'mentionedPersons')
                                        for $i in (1 to array:size($mentionedPersonsArray))
                                        let $key := array:get($mentionedPersonsArray, $i)
                                        return <TEI:person><TEI:persName key="{$key}">{JsonToXml:create_name(database:database-xml-item("Personen", $key))}</TEI:persName></TEI:person>
                                    }
                                </TEI:listPerson>
                            </TEI:particDesc>
                        else ()
                    }

            </TEI:profileDesc>
            }
            </TEI:teiHeader>
            <TEI:facsimile>
                {if(map:get($parsed, 'envelopeFrontImageURL')) then (<TEI:graphic url="{map:get($parsed, 'envelopeFrontImageURL')}"></TEI:graphic>
                )else()
                }
                {if(map:get($parsed, 'envelopeBackImageURL')) then (<TEI:graphic url="{map:get($parsed, 'envelopeBackImageURL')}"></TEI:graphic>
                )else()
                }                {JsonToXml:createImages($transcriptions)}

            </TEI:facsimile>
            <TEI:text>
                <TEI:body>
                    {JsonToXml:createTranscription($transcriptions)}
                </TEI:body>
            </TEI:text>
        </TEI:TEI>
    return $xml
};
