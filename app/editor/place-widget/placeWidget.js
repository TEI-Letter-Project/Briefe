const geonamesUrl = 'http://api.geonames.org/searchJSON';
const geonamesUsername = '';    // A Geonames account name must be set in order to make the place widget work.
const maxPlaceNamesInModal = 25;
const maxSuggestions = 4;

let activeWidget;
let geonamesResults = [];
let suggestions = [];


function addPlaceWidget(widgetId) {

    $.get('../place-widget/placeWidget.html').done(data => {
        $('#' + widgetId).append(data);
        initializePlaceInputFields(widgetId);
    });
}


function initializePlaceInputFields(widgetId) {

    const inputFields = document.getElementById(widgetId).getElementsByClassName('place-input');

    for (let i = 0; i < inputFields.length; i++) {
        if (inputFields.item(i).getAttribute('disabled')) continue;

        inputFields.item(i).addEventListener('keyup', event => updateSuggestions(event));
        inputFields.item(i).addEventListener('focus', event => updateSuggestions(event));
        inputFields.item(i).addEventListener('blur', () => onPlaceInputFieldBlur());
    }
}


function onPlaceInputFieldBlur() {

    removeSuggestionContainer();
    const inputElement = getInputElement();
    if (!inputElement.getAttribute('disabled')) inputElement.value = '';
}


async function updateModalContent() {

    const placeList = $('#placeList');
    placeList.remove();

    const loadingSpinner = $('<div class="loading-spinner-container"><span class="fa fa-spinner fa-pulse fa-2x fa-fw"></span></div>');
    loadingSpinner.appendTo($('#placeModal .modal-body'));
    await loadPlaceList();
    loadingSpinner.remove();
}


function loadPlaceList() {

    const userInput = getInputText();

    return new Promise(resolve => {
        $.get(getUrl(userInput)).done(data => {
            geonamesResults = data.geonames;
            createPlaceList(userInput);
            resolve();
        });
    });
}


function createPlaceList(userInput) {

    const listElement = $('<ul id="placeList" class="list-group" />').appendTo($('#placeModal .modal-body'));

    if (geonamesResults.length === 0) {
        createNoPlacesFoundInfoElement(userInput).appendTo(listElement);
    } else {
        geonamesResults.forEach(geoname => {
            createPlaceListEntry(geoname).appendTo(listElement);
        });
    }
}


function createPlaceListEntry(geoname) {

    const entryElement = $('<div class="place-list-entry" />');
    entryElement.data('geonameId', geoname.geonameId);
    entryElement.data('lat', geoname.lat);
    entryElement.data('lng', geoname.lng);

    entryElement.click(async function() {
        await selectPlaceListEntry($(this).data('geonameId'), $(this).text(), $(this).data('lat'), $(this).data('lng'));
    });

    createNameElement(geoname).appendTo(entryElement);
    createAdditionalInformationElement(geoname).appendTo(entryElement);

    return entryElement;
}


function createNoPlacesFoundInfoElement(userInput) {

    const noPlacesFoundInfoElement = $('<div class="no-places-found-info"</div>');
    noPlacesFoundInfoElement.text('Es wurden keine Orte mit dem Namen "' + userInput + '" gefunden.');

    return noPlacesFoundInfoElement;
}


async function selectPlaceListEntry(geonameId, name, lat, lng) {

    let place = await getPlaceFromDatabase(geonameId);

    if (!place) {
        try {
            place = await addNewPlaceToDatabase(geonameId, name, lat, lng);
        } catch (e) {
            console.error('Failed to add new place to database', e);
            return;
        }
    }

    selectPlace(place.id, place.name);
    $('#placeModal').modal('toggle');
}


function getPlaceFromDatabase(geonameId) {

    return new Promise(resolve => {
        $.getJSON('../place-widget/getPlace.xq', { id: getPlaceId(geonameId) }, data => {
            if (!data || Array.isArray(data.result)) {
                resolve();
            } else {
                resolve(data.result);
            }
        });
    });
}


function createNameElement(geoname) {

    const nameElement = $('<b/>');
    nameElement.text(geoname.name);

    return nameElement;
}


function createAdditionalInformationElement(geoname) {

    const additionalInformationElement = $('<span/>');
    additionalInformationElement.text(' (' + geoname.adminName1 + ', ' + geoname.countryName + ')');

    return additionalInformationElement;
}


function getInputText() {

    return activeWidget.getElementsByTagName('input')[0].value;
}


function getWidgetElement(event) {

    let element = event.target;

    while (!element.classList.contains('place-widget')) {
        element = element.parentElement;
    }

    return element;
}


function getUrl(userInput) {

    return geonamesUrl
        + '?name=' + userInput
        + '&username=' + geonamesUsername
        + '&maxRows=' + maxPlaceNamesInModal
        + '&featureClass=P' // Search for cities & villages only
        + '&lang=de';
}


function addNewPlaceToDatabase(geonameId, name, lat, lng) {

    const place = {
        'id': getPlaceId(geonameId),
        'name': name,
        'coordinates': lat + ' ' + lng
    };

    return new Promise((resolve, reject) => {
        $.ajax({
            url: '../place-widget/addNewPlace.xq',
            type: 'POST',
            data: { json: JSON.stringify(place) },
            dataType: 'xml',
            success: () => resolve(place),
            error: e => reject(e)
        });
    });
}


function selectPlace(id, name) {

    savePlaceIdInPostalObject(id);

    const inputElement = getInputElement();
    inputElement.value = name;
    inputElement.setAttribute('disabled', 'true');
    inputElement.setAttribute('data-place-id', id);

    suggestions = [];
    removeSuggestionContainer();
    addRemoveButton();

    if (getFieldName() === 'mentionedPlaces') addPlaceWidget(activeWidget.parentElement.id);
}


function removePlace() {

    const inputElement = getInputElement();

    removePlaceIdFromPostalObject(inputElement.getAttribute('data-place-id'));

    inputElement.value = '';
    inputElement.removeAttribute('disabled');
    inputElement.removeAttribute('data-place-id');

    removeRemoveButton();
    if (activeWidget.parentElement.children.length > 1) {
        activeWidget.parentElement.removeChild(activeWidget);
        activeWidget = undefined;
    }
}


function savePlaceIdInPostalObject(id) {

    const fieldName = getFieldName();

    if (fieldName === 'mentionedPlaces') {
        postalObject[fieldName].push(id);
    } else {
        postalObject[getFieldName()] = id;
    }
}


function removePlaceIdFromPostalObject(id) {

    const fieldName = getFieldName();

    if (fieldName === 'mentionedPlaces') {
        postalObject[fieldName].splice(postalObject[fieldName].indexOf(id), 1);
    } else {
        postalObject[fieldName] = null;
    }
}


function getFieldName() {

    return activeWidget.parentElement.getAttribute('data-field-name');
}


function updateSuggestions(event) {

    const searchString = event.target.value;
    if (searchString.length === 0) {
        removeSuggestionContainer();
        return;
    }

    activeWidget = getWidgetElement(event);

    $.getJSON('../place-widget/getPlaceSuggestions.xq', { searchString: searchString }, data => {
        const newSuggestions = getSuggestions(data);
        if (haveSuggestionsChanged(newSuggestions) || !$('#placeSuggestionContainer').length) {
            suggestions = newSuggestions;
            removeSuggestionContainer();
            createSuggestionContainerElement().appendTo(activeWidget);
        }
    });
}


function haveSuggestionsChanged(newSuggestions) {

    if (suggestions.length !== newSuggestions.length) return true;

    for (let suggestion of suggestions) {
        if (!newSuggestions.includes(suggestion)) return false;
    }

    return true;
}


function getSuggestions(data) {

    if (!data) return [];

    if (Array.isArray(data.result)) {
        return data.result.length > maxSuggestions ? data.result.slice(0, maxSuggestions) : data.result;
    } else {
        return [data.result];
    }
}


function createSuggestionContainerElement() {

    const suggestionContainerElement = $('<div id="placeSuggestionContainer"/>');

    for (let suggestion of suggestions) {
        createSuggestionElement(suggestion).appendTo(suggestionContainerElement);
    }

    createModalLinkElement().appendTo(suggestionContainerElement);

    return suggestionContainerElement;
}


function createSuggestionElement(suggestion) {

    const suggestionElement = $('<div class="suggestion">');
    suggestionElement.text(suggestion.name);
    suggestionElement.data('placeId', suggestion.id);

    preventTriggeringOfBlurEventForInputElement(suggestionElement);

    suggestionElement.click(function() {
        selectPlace($(this).data('placeId'), $(this).text());
    });

    return suggestionElement;
}


function createModalLinkElement() {

    const modalLinkElement = $('<div class="suggestion place-modal-link" data-toggle="modal" data-target="#placeModal" />');
    modalLinkElement.text('Ort suchen...');

    preventTriggeringOfBlurEventForInputElement(modalLinkElement);

    modalLinkElement.click(() => updateModalContent());

    return modalLinkElement;
}


function preventTriggeringOfBlurEventForInputElement(suggestionElement) {

    suggestionElement.mousedown(event => event.preventDefault());
}


function removeSuggestionContainer() {

    $('#placeSuggestionContainer').remove();
}


function addRemoveButton() {

    createRemoveButtonElement().appendTo(activeWidget);
    getInputElement().classList.add('short-place-input');
}


function removeRemoveButton() {

    activeWidget.removeChild(activeWidget.getElementsByClassName('remove-place-button')[0]);
    getInputElement().classList.remove('short-place-input');
}


function createRemoveButtonElement() {

    const removeButtonElement = $('<div class="btn btn-danger remove-place-button" />');
    $('<span class="glyphicon glyphicon-minus" />').appendTo(removeButtonElement);

    removeButtonElement.click(event => {
        activeWidget = getWidgetElement(event);
        removePlace();
    });

    return removeButtonElement;
}


function getInputElement() {

    return activeWidget.getElementsByClassName('place-input')[0];
}


function getPlaceId(geonameId) {

    return 'o' + geonameId;
}