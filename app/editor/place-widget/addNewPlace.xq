xquery version "3.1";

import module namespace database = "http://exist.db/app/TeiBriefe/modules/database" at "../../modules/database.xqm";
import module namespace PlaceWidget = "http://localhost:8080/exist/apps/briefe/PlaceWidget" at "createPlaceXml.xq";


<div>
    {
        let $json := parse-json(
            request:get-parameter('json', ''),
            map { 'liberal': true(), 'duplicates': 'use-last' }
        )
        let $xml := PlaceWidget:createPlaceXml($json)
        let $store := database:store-xml($xml, map:get($json, 'id'), 'Orte')

        return $xml
    }
</div>
