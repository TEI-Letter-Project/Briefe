module namespace PlaceWidget = "http://localhost:8080/exist/apps/briefe/PlaceWidget";


declare function PlaceWidget:createPlaceXml($json) {

    let $xml :=
        <TEI:TEI xmlns:TEI="http://www.tei-c.org/ns/1.0">
            <TEI:teiHeader>
                <TEI:fileDesc>
                    <TEI:titleStmt>
                        <TEI:title/>
                    </TEI:titleStmt>
                    <TEI:publicationStmt>
                        <TEI:p/>
                    </TEI:publicationStmt>
                </TEI:fileDesc>
                <TEI:profileDesc>
                    <TEI:settingDesc>
                        <TEI:place xml:id="{map:get($json, 'id')}">
                            <TEI:placeName>{map:get($json, 'name')}</TEI:placeName>
                            <TEI:location>
                                <TEI:geo>{map:get($json, 'coordinates')}</TEI:geo>
                            </TEI:location>
                        </TEI:place>
                    </TEI:settingDesc>
                </TEI:profileDesc>
            </TEI:teiHeader>
            <TEI:text>
                <TEI:body>
                    <TEI:p/>
                </TEI:body>
            </TEI:text>
        </TEI:TEI>

    return $xml
};
