xquery version "3.1";

import module namespace database = "http://exist.db/app/TeiBriefe/modules/database" at "../../modules/database.xqm";

declare namespace TEI = "http://www.tei-c.org/ns/1.0";
declare option exist:serialize "method=json media-type=text/javascript";


<root>
    {
        let $id := request:get-parameter('id', '')
        for $place in $database:orte where $place//TEI:place/@xml:id = $id return
            <result>
                <id>{$place//TEI:place/@xml:id/string()}</id>
                <name>{$place//TEI:place/TEI:placeName/text()}</name>
                <coordinates>{$place//TEI:place/TEI:location/TEI:geo/text()}</coordinates>
            </result>
    }
</root>
