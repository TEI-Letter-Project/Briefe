xquery version "3.1";

(:~
: User: jan
: Date: 07.09.18
: Time: 14:46
: To change this template use File | Settings | File Templates.
:)

import module namespace database = "http://exist.db/app/TeiBriefe/modules/database" at "../modules/database.xqm";

<div>
    {


        let $data := request:get-uploaded-file-data('image')
        let $filename := request:get-uploaded-file-name('image')
        let $tokenizedFileName := tokenize($filename, "\.")
        let $uid := database:create-id($tokenizedFileName[1], 'i')
        let $store := database:store-binary($data, $uid, $tokenizedFileName[2])

        return concat($uid,".",$tokenizedFileName[2])
    }
</div>



