var currentTab;
var tabsToAdd = "";
var mentionedPersonsCache = {};
var persons = null;
var personsAsString = null;


//This contains all information of a letter or a postcard
var postalObject = {
    fieldPostNumber: null,
    type: null,
    receiverID: null,
    receiverStreet: null,
    receiverPlace: null,
    receiverHousenumber: null,
    senderID: null,
    senderStreet: null,
    senderPlace: null,
    senderHousenumber: null,
    shippingDate: null,
    stamp: null,
    envelopeFrontImageURL: null,
    envelopeBackImageURL: null,
    transcriptions: [],
    mentionedPersons: [],
    mentionedPlaces: []
};

var files = [];
var fileNames = [];
var envelopeImgs = [];

$(document).ready(function () {
    if ($('#letterContainer').length) {
        setType('letter')
    } else {
        setType('postCard')
    }
    currentTab = 0;
    showTab(currentTab);
    initPostalInfosStep();
    initMetaStep();
});

function setType(type) {
    postalObject.type = type;
}

function initPostalInfosStep() {
    $.get("../templates/postalInfos.html")
        .done((data) => {
            $('#postalInfos').append(data);
            initPlaceWidget();
            createPersonsArray();
            initPersonWidget();

        });

}

function initMetaStep() {
    $.get("../imageSlider/imageSlider.html")
        .done((data) => {
        $('#metaStep').append(data);
});
    $.get("../templates/metaInfosEditor.html")
        .done((data) => {
        $('#metaInfosEditor').append(data);
});
}




function initPlaceWidget() {
    addPlaceWidget('receiverPlaceWidget');
    addPlaceWidget('senderPlaceWidget');
    addPlaceWidget('stampPlaceWidget');

    $.get("../place-widget/placeWidgetModal.html")
        .done((data) => {
        $('#placeModal').append(data);
    });
}

function initPersonWidget() {
    addPersonWidget('receiverPersonWidget', 'receiverID');
    addPersonWidget('senderPersonWidget', 'senderID');


    $.get("../person-widget/personWidgetModal.html")
        .done((data) => {
            $('#personModal').append(data);
            initializePersonWidget();
        });
}



function showTab(n) {
    var x = document.getElementsByClassName('tab');
    x[n].style.display = "block";
    if (n == 0) {
        document.getElementById("prevBtn").style.display = "none";
    } else {
        document.getElementById("prevBtn").style.display = "inline";
    }
    if (n == (x.length - 1)) {
        showDivs(1);
        document.getElementById("nextBtn").innerHTML = "Fertig stellen";
    } else {
        document.getElementById("nextBtn").innerHTML = "Weiter";
    }
    fixStepIndicator(n)
}

function fixStepIndicator(n) {
    var i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
        x[i].className = x[i].className.replace(" active", "");
    }
    x[n].className += " active";
}

function nextPrev(n) {
    // fillPostalObject();
    var x = document.getElementsByClassName("tab");
    if (n === 1 && !validateForm()) return false;
    x[currentTab].style.display = "none";
    if (currentTab === 1 && n > 0) updateContent();
    currentTab = currentTab + n;
    updateProgressBar();
    if (currentTab >= x.length) {
        submit();
        return false;
    }
    showTab(currentTab);
    updatePlaceWidget(n);
}

function validateForm() {
    var tabs, inputs, i, valid = true;
    tabs = document.getElementsByClassName("tab");
    inputs = tabs[currentTab].getElementsByTagName("input");
    for (i = 0; i < inputs.length; i++) {
        if (inputs[i].required && inputs[i].value == "") {
            inputs[i].className += " invalid";
            valid = false;
        }
    }
    if (valid) {
        document.getElementsByClassName("step")[currentTab].className += " finish";
    }
    return valid;
}

function updateProgressBar() {
    var stepSize = $("#stepsBar").children().length;
    var $bar = $("#progress-bar");
    if (currentTab < 2) {
        stepSize = 10;
    }
    $bar.css({ width: (((100 / stepSize) * (currentTab + 1))) + "%" });
    document.getElementById("progress-bar").setAttribute("aria-valuenow", parseFloat($("#progress-bar").attr('aria-valuenow')) + 25);
}

function showPreviewEnvelope(target) {
    var file = document.getElementById(target + "Upload").files[0];
    var output = $("#" + target + "View");
    var reader = new FileReader();
    reader.onload = function (e) {
        output.attr('src', e.target.result);
        envelopeImgs.push(e.target.result);
    };
    reader.readAsDataURL(file);
}

function fileChooser() {
    var newFile = document.getElementById('files').files[0];
    files.push(newFile);
    console.log(files);
    callStoreImages(newFile, "transcriptionImage");
    $('#fileNames').append("<li class=\"uploadedFileListItem\">" + newFile.name + "</li>");
}

function updateContent() {
    $('.mySlides').remove();
    $('.col-md-3').remove('[id^=step]');
    $('[id^=transcriptionArea]').remove();
    var tabsToAdd = "";
    var elementsToAddToSteps = "";
    for (var j = 0, f; f = files[j]; j++) {
        tabsToAdd += "<div class=\"tab row\" id=\"transcriptionArea" + j + "\"></div>";
        elementsToAddToSteps += "<div id=\"step" + j + "\" class=\"col-md-3\"><div class=\"step\"> Seite " + (j + 1) + "</div></div>";
    }
    $(tabsToAdd).insertAfter('#uploadArea');
    $(elementsToAddToSteps).insertAfter("#uploadLettersCol");
    loadTranscriptTemplate();
}

function loadTranscriptTemplate() {
    $.get("../templates/transcriptionTemplate.html").done(data => {
        for (var i = 0, f; f = files[i]; i++) {
            $('#transcriptionArea' + i).append(data);
        }
        loadImages();
        loadMentionedPersonsList();
        addPlaceWidget('mentionedPlacesPlaceWidget');
        addPersonWidget('anotherPersonWidget', 'mentionedPersons');
    });
}

function loadMentionedPersonsList() {
    $('#mentionedPersonsList').empty();
    for (var key in mentionedPersonsCache) {
        if (mentionedPersonsCache.hasOwnProperty(key)) {
            $('#mentionedPersonsList').append('<li>' + mentionedPersonsCache[key] + '</li>');
        }
    }
}

function addPersonToChache(personID, personName) {
    mentionedPersonsCache[personID] = personName;
    console.log(mentionedPersonsCache);
    loadMentionedPersonsList();
}

function updatePlaceWidget(n) {
    if (currentTab > 2 || (currentTab === 2 && n < 0)) {
        const oldContainerTab = n < 0 ? currentTab - 1 : currentTab - 3;
        const newContainerTab = currentTab - 2;

        const oldContainer = document.getElementsByClassName('mentionedPlacesContainer')[oldContainerTab];
        const newContainer = document.getElementsByClassName('mentionedPlacesContainer')[newContainerTab];

        if (newContainer.getElementsByClassName('mentionedPlaces').length > 0) {
            newContainer.removeChild(newContainer.getElementsByClassName('mentionedPlaces')[0]);
        }
        newContainer.appendChild(oldContainer.getElementsByClassName('mentionedPlaces')[0]);
    }
}

// This has to be changed, when images are uploaded into db
function loadImages() {
    var summaryPreview = $('#summaryPreview');
    function readAndPreview(file, index) {
        var currentImg = $('#transcriptionArea' + index).find('.previews')[0];
        var reader = new FileReader();
        reader.addEventListener("load", function () {
            currentImg.src = this.result;
            currentImg.name = fileNames[0];
            fileNames.shift();
            console.log(currentImg);
        }, false);

        reader.addEventListener("load", function () {
            summaryPreview.append("<img src=\"" + this.result + "\" class=\"mySlides previews\"\> ");
        }, false);

        reader.readAsDataURL(file);
    }

    if (files) {
        [].forEach.call(files, readAndPreview);
    }
}

function resetImageFiles() {
    files = []
    $('.uploadedFileListItem').remove();
    $('.mySlides').remove();
    $('.col-md-3').remove('[id^=step]');
    $('[id^=transcriptionArea]').remove();
}

function resetEnvelopeImageFiles(idString) {
    envelopeImgs = []
    $('#' + idString).attr('src', '');
}

function fillPostalObject(fileds) {
    if (document.getElementById("transcription") != null) {
        var transcript = document.getElementById("transcription").value;
        // This can be used, when images are uploaded into db
        var imageToTranscript = document.getElementById("imageToTranscript").name;
        // Placeholder for real Imageurl
        var transcriptionToOverwrite = postalObject.transcriptions.find(trans => trans.imageFileURL === imageToTranscript);
        if (transcriptionToOverwrite) {
            postalObject.transcriptions[transcriptionToOverwrite.index] = { imageFileURL: imageToTranscript, transcriptedText: transcript };
        } else
            postalObject.transcriptions.push({ imageFileURL: imageToTranscript, transcriptedText: transcript });
        console.log(JSON.stringify(postalObject));
    }
}

function submit() {
    var transcriptionTemplates = document.getElementsByClassName("transcriptionTemplate");
    console.log("transcriptionTemplates: " + transcriptionTemplates.length);
    postalObject.envelopeBackImageURL = $("#storeenvelopeBackUpload").attr("filename");
    postalObject.envelopeFrontImageURL = $("#storeenvelopeFrontUpload").attr("filename");
    [].forEach.call(transcriptionTemplates, item => {
        console.log(item.getElementsByClassName("imageToTranscript")[0]);
    postalObject.transcriptions.push(
        {
            imageFileURL: item.getElementsByClassName("imageToTranscript previews")[0].name,
            transcriptedText: item.getElementsByClassName("form-control transcriptionText")[0].value
        }
    );
});

    Object.keys(postalObject).forEach(function (key) {
        if (document.getElementById(key) != null) {
            postalObject[key] = document.getElementById(key).value;
        }
    });
    $.ajax({
        url: "../uploadXml.xq",
        type: "POST",
        data: {json: JSON.stringify(postalObject), type: postalObject.type},
        dataType: "xml",

        success: function(result) {
            const mode = postalObject.type === 'letter' ? 'Briefe' : 'Postkarten';
            const id = result.getElementsByTagName('div')[0].innerHTML;
            location.replace('../../itemview/view.html?mode=' + mode + '&item=' + id);
        }
    });
    console.log(JSON.stringify(postalObject));
};

function callStoreImages(file, name){
    formdata = new FormData();
    if(formdata){
        formdata.append('image', file);
        jQuery.ajax({
            url: "../uploadPictures.xq",
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            success: function(data) {
                $xml = $(data);
                $id = $xml.find("div");
                if (name == "transcriptionImage") {
                    fileNames.push($id.text());
                }
                else {
                    $newElement = $("<span></span>").attr("id", "store" + name).attr("filename", $id.text());
                    $("#storeenvelopes").append($newElement);
                }
            }
        });
    }
}
