xquery version "3.1";
declare namespace personWidget = "test";

import module namespace database = "http://exist.db/app/TeiBriefe/modules/database" at "../../modules/database.xqm";
declare namespace TEI = "http://www.tei-c.org/ns/1.0";



(:~
: User: Viktoriya
: Date: 16.09.18
: Time: 21:48
: To change this template use File | Settings | File Templates.
:)

(:TODO Import Places for Birth and Death from place-Widget:)


declare function personWidget:myStart($jsonString as xs:string, $idno as xs:string){
    (: let $json := replace($jsonString, '\\n', "[/n]") :)

    let $options := map {"liberal" :true(), "duplicates": "use-last"}
    let $parsed := parse-json($jsonString, $options)





        let $xml :=

        <TEI:TEI xmlns:TEI="http://www.tei-c.org/ns/1.0">
            <TEI:teiHeader>
                <TEI:fileDesc>
                    <TEI:titleStmt>
                        <TEI:title>{$idno}</TEI:title>
                    </TEI:titleStmt>
                    <TEI:editionStmt>
                        <TEI:edition/>
                    </TEI:editionStmt>
                    <TEI:publicationStmt>
                        <TEI:p>Publication Information</TEI:p>
                    </TEI:publicationStmt>
                    <TEI:sourceDesc>
                        <TEI:p>Information about the source</TEI:p>
                    </TEI:sourceDesc>
                </TEI:fileDesc>
                <TEI:profileDesc>
                    <TEI:particDesc>
                        <TEI:listPerson>
                            <TEI:person xml:id="{$idno}">


                                <TEI:persName>
                                    <TEI:forename> {map:get($parsed,'forename')}</TEI:forename>
                                    <TEI:surname>
                                        {if(not(empty(map:get($parsed, 'surname'))))
                                        then
                                            <TEI:surname type="birth">{map:get($parsed, 'surname')}</TEI:surname>
                                            else()}

                                        {if(not(empty(map:get($parsed, 'surname_married'))))
                                            then <TEI:surname type="married">{map:get($parsed, 'surname_married')}</TEI:surname>
                                            else()}
                                    </TEI:surname>
                                    <TEI:addName>{map:get($parsed,'addName')}</TEI:addName>
                                    <TEI:roleName type="military">{map:get($parsed,'roleName')}</TEI:roleName>
                                </TEI:persName>


                                <!--nach ISO IEC 5218: 0 = not known, 1 = male, 2 = female, 9 = not applicable. -->
                                <TEI:sex value="{map:get($parsed,'sex')}">
                                </TEI:sex>

                                <TEI:birth when="{map:get($parsed,'birth')}">
                                    <TEI:date>{map:get($parsed,'birth')}</TEI:date>
                                    <TEI:name type="place"></TEI:name>
                                </TEI:birth>

                                <TEI:death when="{map:get($parsed,'death')}">
                                    <TEI:date>{map:get($parsed,'death')}</TEI:date>
                                    <TEI:name type="place"></TEI:name>
                                </TEI:death>

                                <TEI:occupation> {map:get($parsed,'occupation')} </TEI:occupation>

                                <TEI:affiliation notAfter="99999999" notBefore="99999999">
                                    <TEI:orgName>{map:get($parsed,'affiliation')}</TEI:orgName>
                                </TEI:affiliation>

                            </TEI:person>

                            <TEI:listRelation>
                                <TEI:relation name="" type="" key=""></TEI:relation>
                            </TEI:listRelation>

                        </TEI:listPerson>
                    </TEI:particDesc>
                </TEI:profileDesc>
            </TEI:teiHeader>
            <TEI:text><TEI:body><TEI:p></TEI:p></TEI:body></TEI:text>




        </TEI:TEI>
    return $xml
};



<div>
{
    let $jsonstring := request:get-parameter('xml','')
    let $bla := util:log("ERROR", $jsonstring)
    let $name := database:create-id("newXml", "p")
    let $xml := personWidget:myStart($jsonstring, $name)
    let $store := database:store-xml($xml, $name, "Personen")
    return $xml
}
</div>



