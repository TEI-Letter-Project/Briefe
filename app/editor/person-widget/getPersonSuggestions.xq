xquery version "3.1";

import module namespace database = "http://exist.db/app/TeiBriefe/modules/database" at "../../modules/database.xqm";

declare namespace TEI = "http://www.tei-c.org/ns/1.0";
declare option exist:serialize "method=json media-type=text/javascript";


<root>
    {
        let $searchString := lower-case(request:get-parameter('searchString', ''))
        for $person in $database:personen where starts-with(lower-case($person//TEI:person/TEI:personName/TEI:surname), $searchString) return
            <result>
                <id>{$person//TEI:person/@xml:id/string()}</id>
                <surname>{$person//TEI:person/TEI:persName/TEI:surname/TEI:surname/string()}</surname>
                <forename>{$person//TEI:person/TEI:persName/TEI:forename/text()}</forename>
                <birth>{$person//TEI:person/TEI:birth/TEI:date/text()}</birth>
                <death>{$person//TEI:person/TEI:death/TEI:date/text()}</death>

            </result>
    }
</root>
