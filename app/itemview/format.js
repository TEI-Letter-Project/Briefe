//Code by Gwenlyn Tiedemann

$(document).ready(function(){

    $(document).tooltip();


//Darstellung Brief/Postkarte - Zweispaltige Anzeige laden

        $("#text-active").show();
        $("#image-active").show();

       
        $("#tabsId-l").on("click", "li", function(evt) {
            evt.preventDefault();
            var id = $(this).data("tabContent");
            $("#tabsContentId-l")
            .find(".tab-content[data-tab-content='" + id + "']").show()
            .siblings().hide();
        });
        
         $("#tabsId-r").on("click", "li", function(evt) {
            evt.preventDefault();
            var id = $(this).data("tabContent");
            $("#tabsContentId-r")
            .find(".tab-content[data-tab-content='" + id + "']").show()
            .siblings().hide();
        });
        
        $("ul.nav li").on( 'click', function() {
            $( this ).parent().find( 'li.active' ).removeClass( 'active' );
            $( this ).addClass( 'active' );
      });
    

    
// Personen - Zugehörige Korrespondenz onclick laden
    var id = $("#person-content.class").hide();
       $("#person-content").hide();
        $(".person").on("click", function(e){
        e.preventDefault();
        console.log(this.id);
        var person = this.id;
        $("."+person).toggle();
        });


        //Image Selector Funktionen

    $(".imageSelector").click(
        function(){
            var clickedSelector = this;
            var side = $(clickedSelector).attr('side');
            $(".mainImage."+side).attr("src", $(clickedSelector).attr('src'))
        });


    wheelzoom(document.querySelectorAll(".mainImage"));

});

