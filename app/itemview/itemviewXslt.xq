xquery version "3.0";

(:~
: User: jan
: Date: 17.09.18
: Time: 13:43
: To change this template use File | Settings | File Templates.
:)

module namespace itemviewXslt = "http://exist/db/apps/Briefe/itemview/itemviewXslt";

declare variable $itemviewXslt:xsl := <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:TEI="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs TEI" xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="2.0">
    <xsl:output method="xhtml" indent="no"/>
    <xsl:param name="img-root"/>
    <xsl:param name="side"/>
    <xsl:param name="app-root"/>
    <xsl:param name="timelinelink"/>
    <xsl:param name="xmllink"/>
    <xsl:template match="/">
        <div id="meta-text">
            <xsl:apply-templates/>
            <div class="tab-content" data-tab-content="context">
                <a class="timeline-link"><xsl:attribute name="href" select="$timelinelink"/>In der Zeitleiste anzeigen</a><br/>
                <a class="xml-link"><xsl:attribute name="href" select="$xmllink"/>XML herunterladen</a>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="teiHeader">
        <div class="tab-content" data-tab-content="meta-text">
            <div id="meta-text">
                <xsl:apply-templates/>
            </div>
        </div>
    </xsl:template>


    <xsl:template match="facsimile">
        <div class="tab-content" data-tab-content="images">
            <div id="imageContainer">
                <img id="mainimage">
                    <xsl:attribute name="class" select="concat(&#34;mainImage &#34;, $side)"/>
                    <xsl:attribute name="src" select="concat($img-root, '/', ./graphic[1]/@url)"/>
                </img>
            </div>
            <div class="selectorDiv">
                <xsl:for-each select="./graphic">
                    <xsl:choose>
                        <xsl:when test="position() = 1">
                            <img class="imageSelector active">
                                <xsl:attribute name="side" select="$side"/>
                                <xsl:attribute name="id" select="position()"/>
                                <xsl:attribute name="src" select="concat($img-root, '/', ./@url)"/>
                            </img>
                        </xsl:when>
                        <xsl:otherwise>
                            <img class="imageSelector">
                                <xsl:attribute name="side" select="$side"/>
                                <xsl:attribute name="id" select="position()"/>
                                <xsl:attribute name="src" select="concat($img-root, '/', ./@url)"/>
                            </img>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="graphic"> </xsl:template>


    <xsl:template match="body">
        <div class="tab-content" data-tab-content="text" id="text-active">
            <div id="meta-text">
                <xsl:apply-templates/>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="respStmt">
        <em>
            <xsl:value-of select="./resp/text()"/> by <xsl:value-of select="./name/text()"/>
        </em>
    </xsl:template>

    <xsl:template match="title">
        <h4>
            <xsl:value-of select="./date/text()"/>
        </h4>
        <h4 class="meta-bold">
            <xsl:value-of select="./text()"/>
        </h4>
    </xsl:template>

    <xsl:template match="repository">
        <div id="archiveDiv">
            <span id="archiveLabel" class="meta-bold">Archiv: </span>
            <span id="archiveName">
                <xsl:value-of select=".//text()"/>
            </span>
        </div>
    </xsl:template>

    <xsl:template match="msIdentifier">
        <div id="IdDiv">
            <span id="IdLabel" class="meta-bold">Id:</span>
            <span id="IdName">
                <xsl:value-of select="./idno/text()"/>
            </span>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="altIdentifier[@type='FPNR']">
        <span id="FPNRlabel" class="meta-bold">Feldpostnummber: </span>
        <span id="FPNR">
            <xsl:value-of select="./idno/text()"/>
        </span>
    </xsl:template>
    <xsl:template match="correspAction[@type = 'sent']">
        <span class="meta-bold">Absender: </span>
        <ul class="list">
            <xsl:apply-templates/>
        </ul>
    </xsl:template>

    <xsl:template match="correspAction[@type = 'received']">
        <span class="meta-bold">Empfänger: </span>
        <ul class="list">
            <xsl:apply-templates/>
        </ul>
    </xsl:template>

    <xsl:template match="listPlace">
        <div>
            <span id="listPlaceLabel" class="meta-bold">Erwähnte Orte: </span>
            <ul>
                <xsl:apply-templates/>
            </ul>
        </div>
    </xsl:template>

    <xsl:template match="listPerson">
        <div>
            <span id="listPersonLabel" class="meta-bold">Erwähnte Personen: </span>
            <ul>
                <xsl:apply-templates/>
            </ul>
        </div>
    </xsl:template>

    <xsl:template match="persName">
        <li>
            <a href="#">
                <xsl:value-of select="./text()"/>
            </a>
        </li>
    </xsl:template>

    <xsl:template match="settlement">
        <li>
            <xsl:value-of select="./text()"/>
        </li>
    </xsl:template>

    <xsl:template match="date">
        <li>
            <xsl:value-of select="./text()"/>
        </li>
    </xsl:template>

    <xsl:template match="div">
        <div class="articel">
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="lb">
        <br>
            <xsl:attribute name="id">
                <xsl:text>n-</xsl:text>
                <xsl:value-of select="./attribute()"/>
            </xsl:attribute>
        </br>
        <span class="numberSpan">
            <xsl:value-of select="./attribute()"/>
        </span>
    </xsl:template>

    <xsl:template match="body//opener/text()">
        <xsl:value-of select="."/>
    </xsl:template>

    <xsl:template match="body//opener/dateline//text()">
        <xsl:value-of select="."/>
    </xsl:template>

    <xsl:template match="body//date">
        <xsl:value-of select="."/>
    </xsl:template>

    <xsl:template match="body//opener//salute//text()">
        <xsl:value-of select="."/>
    </xsl:template>

    <xsl:template match="body//p//text()">
        <xsl:value-of select="."/>
    </xsl:template>

    <xsl:template match="body//closer">
        <p id="closer">
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <xsl:template match="body//closer//text()" name="closertext">
        <xsl:value-of select="."/>
    </xsl:template>

    <xsl:template match="body//rs">
        <xsl:variable name="rs-content" select="."/>
        <xsl:variable name="rb-before" select="./preceding-sibling::lb[1]"/>
        <span id="hili">
            <xsl:if test="./@subtype">
                <xsl:attribute name="title" select="./@subtype"/></xsl:if>
            <xsl:variable name="count" select="count(tokenize($rs-content, '\n'))"/>
            <xsl:for-each select="tokenize($rs-content, '\n')">
                <xsl:value-of select="."/>
                <xsl:if test="position() &lt; $count">
                    <br>
                        <xsl:attribute name="id">
                            <xsl:text>n-</xsl:text>
                            <xsl:value-of select="$rb-before/attribute::n + 1"/>
                        </xsl:attribute>
                    </br>
                    <span class="numberSpan">
                        <xsl:value-of select="$rb-before/attribute::n + 1"/>
                    </span>
                </xsl:if>
            </xsl:for-each>
        </span>
    </xsl:template>

    <xsl:template match="pb">
        <span class="pbSpan">Neue Seite</span>
    </xsl:template>

    <xsl:template match="body//choice">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="abbr">
        <span id="hili" class="tool">
            <xsl:attribute name="title" select="../expan/text()"/>
            <xsl:value-of select="."/>
        </span>
    </xsl:template>

    <xsl:template match="expan"/>
    <xsl:template match="text()"> </xsl:template>


    <xsl:template match="div[@type = 'destination']">
        <div class="address">
            <xsl:apply-templates/>
        </div>
    </xsl:template>


    <xsl:template match="rs">
        <span id="hili">
            <xsl:value-of select="."/>
        </span>
    </xsl:template>

    <xsl:template match="address">
        <p>Adresse: <ul class="list">
            <xsl:apply-templates/>
        </ul>
        </p>
    </xsl:template>
    <xsl:template match="addrLine">
        <li>
            <xsl:value-of select="."/>
        </li>
    </xsl:template>

</xsl:stylesheet>;