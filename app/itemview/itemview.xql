xquery version "3.1";

(:~
: User: jan
: Date: 10.08.18
: Time: 00:38
: To change this template use File | Settings | File Templates.
:)

module namespace itemview = "http://exist/db/apps/TeiBriefe/itemview/itemview";

import module namespace database = "http://exist.db/app/TeiBriefe/modules/database" at "database.xqm";
import module namespace helpers="http://exist.db/app/TeiBriefe/modules/helpers" at "helpers.xqm";
import module namespace itemviewXslt = "http://exist/db/apps/Briefe/itemview/itemviewXslt" at "itemviewXslt.xq";
declare namespace TEI = "http://www.tei-c.org/ns/1.0";


declare variable $itemview:mode := request:get-parameter("mode", "");
declare variable $itemview:itemname := request:get-parameter("item", "");

declare variable $itemview:item := database:database-xml-item($itemview:mode, $itemview:itemname);




declare function itemview:headTitle($node as node(), $model as map(*)){<title>{string($itemview:item//TEI:titleStmt/TEI:title)}</title>};

declare function itemview:title($node as node(), $model as map(*)) {$itemview:item//TEI:titleStmt/TEI:title/text()};
declare function itemview:idno($node as node(), $mode as map(*)){$itemview:item//TEI:msIdentifier/TEI:idno/text()};

declare function itemview:timelineLink($node as node(), $mode as map(*)) {
    <a href="{$helpers:app-root}/timeline/view.html?item={$itemview:item//TEI:msIdentifier/TEI:idno/text()}" class="timeline-link">
        In der Zeitleiste anzeigen
    </a>
};


declare function itemview:LetterContent($node as node(), $model as map(*), $side as xs:string){

    let $params := <parameters>
        <param name="img-root" value="{$database:database-image-collection-link}"/>
        <param name="side" value="{$side}"/>
        <param name="app-root" value="{$helpers:app-root}"/>
        <param name="timelinelink" value="{concat($helpers:app-root,"/timeline/view.html?item=", $itemview:item//TEI:msIdentifier/TEI:idno/text())}"/>
        <param name="xmllink" value="{concat($database:database-xml-collection-link,"/",$itemview:mode,"/",$itemview:itemname,".xml")}"/>
    </parameters>

    (:let $xsl := database:database-xsl-item("transformLetterContent") :)
    let $xsl := $itemviewXslt:xsl

    let $newView := transform:transform($itemview:item, $xsl, $params)

    return $newView
};

