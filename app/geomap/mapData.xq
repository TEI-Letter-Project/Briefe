xquery version "3.1";

import module namespace database = "http://exist.db/app/TeiBriefe/modules/database" at "../modules/database.xqm";

declare namespace TEI = "http://www.tei-c.org/ns/1.0";
declare option exist:serialize "method=json media-type=text/javascript";


<root>
    { for $letter in $database:briefe return
        <result>
            { for $place in $database:orte where $place//TEI:place/@xml:id = $letter//TEI:teiHeader/TEI:profileDesc/TEI:correspDesc/TEI:correspAction[@type="sent"]/TEI:settlement/@key return
                <sentPlace>
                    <id>{$place//TEI:teiHeader/TEI:profileDesc/TEI:settingDesc/TEI:place/@xml:id/string()}</id>
                    <name>{$place//TEI:teiHeader/TEI:profileDesc/TEI:settingDesc/TEI:place/TEI:placeName/text()}</name>
                    <coordinates>
                        <lat>{tokenize($place//TEI:teiHeader/TEI:profileDesc/TEI:settingDesc/TEI:place/TEI:location/TEI:geo/text(), '\s')[0]}</lat>
                        <lng>{tokenize($place//TEI:teiHeader/TEI:profileDesc/TEI:settingDesc/TEI:place/TEI:location/TEI:geo/text(), '\s')[1]}</lng>
                    </coordinates>
                </sentPlace>
            }
            { for $place in $database:orte where $place//TEI:place/@xml:id = $letter//TEI:teiHeader/TEI:profileDesc/TEI:correspDesc/TEI:correspAction[@type="received"]/TEI:settlement/@key return
                <receivedPlace>
                    <id>{$place//TEI:teiHeader/TEI:profileDesc/TEI:settingDesc/TEI:place/@xml:id/string()}</id>
                    <name>{$place//TEI:teiHeader/TEI:profileDesc/TEI:settingDesc/TEI:place/TEI:placeName/text()}</name>
                    <coordinates>
                        <lat>{tokenize($place//TEI:teiHeader/TEI:profileDesc/TEI:settingDesc/TEI:place/TEI:location/TEI:geo/text(), '\s')[1]}</lat>
                        <lng>{tokenize($place//TEI:teiHeader/TEI:profileDesc/TEI:settingDesc/TEI:place/TEI:location/TEI:geo/text(), '\s')[2]}</lng>
                    </coordinates>
                </receivedPlace>
            }
            <id>{$letter//TEI:teiHeader/TEI:fileDesc/TEI:sourceDesc/TEI:msDesc/TEI:msIdentifier/TEI:idno/text()}</id>
            <title>{$letter//TEI:teiHeader/TEI:fileDesc/TEI:titleStmt/TEI:title/text()}</title>
            <mode>Briefe</mode>
        </result>
    }
    { for $postcard in $database:postkarten return
        <result>
            { for $place in $database:orte where $place//TEI:place/@xml:id = $postcard//TEI:teiHeader/TEI:profileDesc/TEI:correspDesc/TEI:correspAction[@type="sent"]/TEI:settlement/@key return
                <sentPlace>
                    <id>{$place//TEI:teiHeader/TEI:profileDesc/TEI:settingDesc/TEI:place/@xml:id/string()}</id>
                    <name>{$place//TEI:teiHeader/TEI:profileDesc/TEI:settingDesc/TEI:place/TEI:placeName/text()}</name>
                    <coordinates>
                        <lat>{tokenize($place//TEI:teiHeader/TEI:profileDesc/TEI:settingDesc/TEI:place/TEI:location/TEI:geo/text(), '\s')[0]}</lat>
                        <lng>{tokenize($place//TEI:teiHeader/TEI:profileDesc/TEI:settingDesc/TEI:place/TEI:location/TEI:geo/text(), '\s')[1]}</lng>
                    </coordinates>
                </sentPlace>
            }
            { for $place in $database:orte where $place//TEI:place/@xml:id = $postcard//TEI:teiHeader/TEI:profileDesc/TEI:correspDesc/TEI:correspAction[@type="received"]/TEI:settlement/@key return
                <receivedPlace>
                    <id>{$place//TEI:teiHeader/TEI:profileDesc/TEI:settingDesc/TEI:place/@xml:id/string()}</id>
                    <name>{$place//TEI:teiHeader/TEI:profileDesc/TEI:settingDesc/TEI:place/TEI:placeName/text()}</name>
                    <coordinates>
                        <lat>{tokenize($place//TEI:teiHeader/TEI:profileDesc/TEI:settingDesc/TEI:place/TEI:location/TEI:geo/text(), '\s')[1]}</lat>
                        <lng>{tokenize($place//TEI:teiHeader/TEI:profileDesc/TEI:settingDesc/TEI:place/TEI:location/TEI:geo/text(), '\s')[2]}</lng>
                    </coordinates>
                </receivedPlace>
            }
            <id>{$postcard//TEI:teiHeader/TEI:fileDesc/TEI:sourceDesc/TEI:msDesc/TEI:msIdentifier/TEI:idno/text()}</id>
            <title>{$postcard//TEI:teiHeader/TEI:fileDesc/TEI:titleStmt/TEI:title/text()}</title>
            <mode>Postkarten</mode>
        </result>
    }
</root>