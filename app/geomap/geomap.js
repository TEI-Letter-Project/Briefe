var map = L.map('map').setView([50.8476, 11.3564], 6);

L.tileLayer('https://maps.tilehosting.com/styles/streets/{z}/{x}/{y}.png?key=06S4rAJt6u44LDwlZeMd',{
    attribution: '<a href="https://www.maptiler.com/license/maps/" target="_blank">© MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">© OpenStreetMap contributors</a>',
    crossOrigin: true
}).addTo(map);

let markerDefinitions = [];

$.getJSON('mapData.xq', data => {

    const items = data.result;

    for (let item of items) {
        if (item.sentPlace) addToMarkers(item.sentPlace, item.id, item.title, item.mode, true);
        if (item.receivedPlace) addToMarkers(item.receivedPlace, item.id, item.title, item.mode, false);
    }

    console.log(markerDefinitions);

    for (let markerDefinition of markerDefinitions) {
        createMarker(markerDefinition);
    }
});


function addToMarkers(place, id, title, mode, sent) {

    let marker = markerDefinitions.find(marker => marker.placeId === place.id);
    if (!marker) {
        marker = {
            placeId: place.id,
            placeName: place.name,
            coordinates: place.coordinates,
            items: []
        };
        markerDefinitions.push(marker);
    }

    marker.items.push({
        id: id,
        title: title.trim(),
        mode: mode,
        sent: sent
    })
}


function createMarker(markerDefinition) {

    const marker = L.marker([markerDefinition.coordinates.lat, markerDefinition.coordinates.lng]);

    let modalContent = '<p><b>' + markerDefinition.placeName + '</b></p>';

    for (let item of markerDefinition.items) {
        modalContent += '<p><a href="../itemview/view.html?mode=' + item.mode + '&item=' + item.id + '">'
            + item.title;
        modalContent += '</a>';
        modalContent += item.sent ? ' (gesendet)' : ' (empfangen)' ;
        modalContent += '</p>';
    }

    const popup = L.popup({ maxWidth: 500 });
    popup.setContent(modalContent);
    marker.bindPopup(popup).openPopup();
    marker.addTo(map);
}