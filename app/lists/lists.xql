xquery version "3.0";

(:~
: User: jan
: Date: 09.08.18
: Time: 11:58
: To change this template use File | Settings | File Templates.
:)

module namespace lists = "http://exist/db/apps/TeiBriefe/lists/lists";

import module namespace database = "http://exist.db/app/TeiBriefe/modules/database" at "database.xqm";
declare namespace TEI = "http://www.tei-c.org/ns/1.0";

declare function lists:createTableRowsForLetters($node as node(), $model as map(*)){

    let $uri-modus := request:get-parameter("modus","Briefe")


    let $collection :=
        if ($uri-modus = "Briefe") then $database:briefe
        else $database:postkarten

    return <tbody> {
        for $item in $collection
        return <tr class="row list-row" data-href="../itemview/view.html?mode={$uri-modus}&amp;item={$item//TEI:msIdentifier/TEI:idno/text()}">
                 <td><img class="thumbnail" src="{concat($database:database-image-collection-link, "/", $item//TEI:graphic[1]/@url)}"/></td>
                 <td>{$item//TEI:titleStmt/TEI:title/text()}</td>
                 <td>{$item//TEI:correspAction/TEI:date/text()}</td>
                 <td>{$item//TEI:idno/text()}</td>
              </tr>
                }
            </tbody>
};
